package com.maury.fragmentstest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class NameDetailActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_detail);

        Intent intent = getIntent();
        String name = intent.getStringExtra(MainActivity.TAG_NAME);
        NameDetailFragment fragment = (NameDetailFragment) getFragmentManager().findFragmentById(R.id.fragment_detail);
        fragment.setName(name);
    }
}
