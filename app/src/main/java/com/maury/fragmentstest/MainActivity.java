package com.maury.fragmentstest;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends FragmentActivity {
    public static final String TAG_NAME = "name";

    private static final ArrayList<String> names = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText txt_name = (EditText) findViewById(R.id.edit_name);
        final ListView list_names = (ListView) findViewById(R.id.list_of_names);
        Button btn_submit = (Button) findViewById(R.id.btn_submit);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.list_item, android.R.id.text1, names);

        list_names.setAdapter(adapter);

        list_names.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = adapter.getItem(position);
                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                    Intent action = new Intent(getApplicationContext(), NameDetailActivity.class);
                    action.putExtra(MainActivity.TAG_NAME, name);
                    startActivity(action);
                }else{
                    NameDetailFragment fragment = (NameDetailFragment) getFragmentManager().findFragmentById(R.id.fragment_detail);
                    fragment.setName(name);
                }
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txt_name.getText().toString();
                if(!names.contains(name)){
                    names.add(name);
                    adapter.notifyDataSetChanged();
                }
            }
        });

    }
}
